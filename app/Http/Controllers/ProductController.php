<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
use Stripe\Charge;
//use Illuminate\Session\Store;
//use Illuminate\Support\Facades\Storage;


class ProductController extends Controller
{
    public function getIndex()
    {
      //  Session::forget('cart');
        $product = Product::all();
        return view('shop.index', ['products' => $product]);
    }

    public function getAddToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);  // OR Session::put()->('cart', $cart);
      //  dd($request->session()->get('cart'));
        return redirect()->route('product.index');
    }

    public function getReduceByOne($id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne( $id);

        if (count($cart->items) > 0){
            Session::put('cart', $cart);  // OR Session::put()->('cart', $cart);
        } else {
            Session::forget('cart');
        }
        //  dd($request->session()->get('cart'));
        return redirect()->route('product.shoppingCart');
    }

    public function getRemoveItem($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem( $id);

        if (count($cart->items) > 0){
            Session::put('cart', $cart);  // OR Session::put()->('cart', $cart);
        } else {
            Session::forget('cart');
        }


        return redirect()->route('product.shoppingCart');
    }

    public function getCart()
    {
        if (!Session::has('cart')){
            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('shop.shopping-cart', ['products' => $cart->items,
           'totalPrice' => $cart->totalPrice ] );

    }

    public function getCheckout()
    {
        if (!Session::has('cart')){
            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;
        return view('shop.checkout', ['total' => $total]);

    }

    public function postCheckout(Request $request)
    {
        if (!Session::has('cart')){
            return redirect()->route('shop.shoppingCart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        // Payment Integration Method
/*        Stripe::setApiKey('sk_test_KGJzRU8FLu72bw1MJXSXTu7S00gkfjCZCL');
        try {
          $charge = Charge::create(array(
                "amount"      => $cart->totalPrice * 100,
                "currency"    => "usd",
                "source"      => $request->input('stripeToken'),
                "description" => "Test Charge"
            ));

          $order             = new Order();
          $order->cart       = serialize($cart);
          $order->address    = $request->input('address');
          $order->name       = $request->input('name');
          $order->payment_id = $charge->id;

          Auth::User()->orders()->save($order);

        } catch (\Exception $e){
            return redirect()->route('checkout')->with('error', $e->getMessage());
        }*/


        $order             = new Order();
        $order->cart       = serialize($cart);
        $order->address    = $request->input('address');
        $order->name       = $request->input('name');
        $order->payment_id = '123456789';

        Auth::User()->orders()->save($order);


        Session::forget('cart');
        return redirect()->route('product.index')->with('success', 'Successfully purchased products!');



    }



}
