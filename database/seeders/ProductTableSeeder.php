<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product([
            'image_path'  => '...',
            'title'       => 'Super Man',
            'category_id' => 1,
            'brand_id'    => 1,
            'price'       => 4000,
            'quantity'    => 6,
            'description' => 'Super cool top'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path'  => '...',
            'title'       => 'Trendy Flat',
            'category_id' => 1,
            'brand_id'    => 1,
            'price'       => 5000,
            'quantity'    => 9,
            'description' => 'Nice flats'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path'  => '...',
            'title'       => 'Minnie Ring',
            'category_id' => 1,
            'brand_id'    => 1,
            'price'       => 3000,
            'quantity'    => 12,
            'description' => 'You would love it'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path'  => '...',
            'title'       => 'My Gold',
            'category_id' => 1,
            'brand_id'    => 1,
            'price'       => 8000,
            'quantity'    => 10,
            'description' => 'Shinny everywhere'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path'  => '...',
            'title'       => 'Gbemisola Sandal',
            'category_id' => 1,
            'brand_id'    => 1,
            'price'       => 11000,
            'quantity'    => 22,
            'description' => 'For your casual Friday'
        ]);
        $product->save();

    }
}
