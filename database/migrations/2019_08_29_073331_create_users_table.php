<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
<<<<<<< HEAD:database/migrations/2019_08_29_073331_create_users_table.php
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('email');
            $table->string('first_name');
            $table->string('last_name');
=======
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
>>>>>>> dd8111a32dc1879a2883441265ae8d0fb10293fd:database/migrations/2014_10_12_000000_create_users_table.php
            $table->string('password');
            $table->rememberToken();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
