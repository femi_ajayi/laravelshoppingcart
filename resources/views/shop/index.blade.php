@extends('layouts.master')


@section('title')
    MindBloom Shopping Cart
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
                <div id="charge-message" class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>
        @endif

    @foreach($products->chunk(3) as $productChunk)
        <div class="row">
            @foreach($productChunk as $product)
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img src="testImage/Breach.jpg" alt="..." {{--style="max-height: 150px"--}} class="img-responsive">
                        <div class="caption">
                            <h3> {{ $product->title }} </h3>
                            <p class="description">
{{--                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet,
                                consequatur
                                deserunt dolor ea illum
                                laudantium magni modi non perferendis quas recusandae similique tempore tenetur unde
                                voluptas,
                                voluptatibus voluptatum! Molestiae, sint?--}}                   {{ $product->description }}
                            </p>
                            <div class="clearfix">
                                <div class="pull-left price">${{ $product->price }}</div>
                                <a href="{{ route('product.addToCart', ['id' => $product->id]) }}"  class="btn btn-success pull-right" role="button">Add to Cart </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach



            {{--            <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="testImage/Breach.jpg" alt="..." --}}{{--style="max-height: 150px"--}}{{-- class="img-responsive">
                                <div class="caption">
                                    <h3> Product Title</h3>
                                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, consequatur
                                        deserunt dolor ea illum
                                        laudantium magni modi non perferendis quas recusandae similique tempore tenetur unde voluptas,
                                        voluptatibus voluptatum! Molestiae, sint?
                                    </p>
                                    <div class="clearfix">
                                        <div class="pull-left price">$12</div>
                                        <a href="#" class="btn btn-success pull-right" role="button">Add to Cart </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="testImage/Breach.jpg" alt="..." --}}{{--style="max-height: 150px"--}}{{-- class="img-responsive">
                                <div class="caption">
                                    <h3> Product Title</h3>
                                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, consequatur
                                        deserunt dolor ea illum
                                        laudantium magni modi non perferendis quas recusandae similique tempore tenetur unde voluptas,
                                        voluptatibus voluptatum! Molestiae, sint?
                                    </p>
                                    <div class="clearfix">
                                        <div class="pull-left price">$12</div>
                                        <a href="#" class="btn btn-success pull-right" role="button">Add to Cart </a>
                                    </div>
                                </div>
                            </div>
                        </div>--}}


        </div>
    @endforeach




    {{-- <div class="row">


         <div class="col-sm-6 col-md-4">
             <div class="thumbnail">
                 <img src="testImage/Breach.jpg" alt="..." --}}{{--style="max-height: 150px"--}}{{-- class="img-responsive">
                 <div class="caption">
                     <h3> Product Title</h3>
                     <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, consequatur deserunt dolor ea illum
                         laudantium magni modi non perferendis quas recusandae similique tempore tenetur unde voluptas,
                         voluptatibus voluptatum! Molestiae, sint?
                     </p>
                     <div class="clearfix">
                         <div class="pull-left price">$12</div>
                         <a href="#" class="btn btn-success pull-right" role="button">Add to Cart </a>
                     </div>
                 </div>
             </div>
         </div>

         <div class="col-sm-6 col-md-4">
             <div class="thumbnail">
                 <img src="testImage/Breach.jpg" alt="..." --}}{{--style="max-height: 150px"--}}{{-- class="img-responsive">
                 <div class="caption">
                     <h3> Product Title</h3>
                     <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, consequatur deserunt dolor ea illum
                         laudantium magni modi non perferendis quas recusandae similique tempore tenetur unde voluptas,
                         voluptatibus voluptatum! Molestiae, sint?
                     </p>
                     <div class="clearfix">
                         <div class="pull-left price">$12</div>
                         <a href="#" class="btn btn-success pull-right" role="button">Add to Cart </a>
                     </div>
                 </div>
             </div>
         </div>

         <div class="col-sm-6 col-md-4">
             <div class="thumbnail">
                 <img src="testImage/Breach.jpg" alt="..." --}}{{--style="max-height: 150px"--}}{{-- class="img-responsive">
                 <div class="caption">
                     <h3> Product Title</h3>
                     <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, consequatur deserunt dolor ea illum
                         laudantium magni modi non perferendis quas recusandae similique tempore tenetur unde voluptas,
                         voluptatibus voluptatum! Molestiae, sint?
                     </p>
                     <div class="clearfix">
                         <div class="pull-left price">$12</div>
                         <a href="#" class="btn btn-success pull-right" role="button">Add to Cart </a>
                     </div>
                 </div>
             </div>
         </div>


     </div>--}}




@endsection

