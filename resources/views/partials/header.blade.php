<?php

?>


<header>

    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">MindBloom</a>


            </div>


            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


                <form method="post" action="#" id="logoutForm">
                    <ul class="nav navbar-nav">

{{--                        <li><a href="#">Dashboard</a></li>
                        <li><a href="#">admin</a></li>--}}

                    </ul>

                    <ul class="nav navbar-nav navbar-right">

                        <li><a href="{{ route('product.shoppingCart') }}"> <i class="fa fa-shopping-cart"></i> Shopping Cart
                                <span class="badge">
                                    {{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}
                                </span>
                            </a>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user"></i>  User Management<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @if(Auth::check())
                                    <li><a href="{{ route('user.profile') }}"> User Profile </a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{ route('user.logout') }}">Sign Out</a></li>
                                    @else
                                    <li><a href="{{ route('user.signup') }}"> Sign Up</a></li>
                                    <li><a href="{{ route('user.signin') }}"> Sign In</a></li>
                                    @endif
{{--                                <li><a href="{{ route('user.signup') }}"> Sign Up</a></li>
                                <li><a href="{{ route('user.signin') }}"> Sign In</a></li>--}}

                                <!--
                                <li><a href="javascript:document.getElementById('logoutForm').submit()">Sign Out</a></li>
                                -->
                            </ul>
                        </li>


                        {{--<li><a href="#">Log In</a></li>--}}

                    </ul>
                </form>



            </div>


        </div>

    </nav>

</header>





