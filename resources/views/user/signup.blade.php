@extends('layouts.master')

@section('title')
    Sign Up - MindBloom
@endsection


@section('content')


    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h1>Sign Up</h1>
            @include('partials.message-block')
            {{--        @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                    @endif--}}
            <form action="{{ route('user.signup') }}" method="post">
                <div class="form-group">
                    <label for="email"> E-mail</label>
                    <input class="form-control" type="text" name="email" id="email" value="" >
                </div>

                <div class="form-group">
                    <label for="email">First Name</label>
                    <input class="form-control" type="text" name="first_name" id="first_name" value="" >
                </div>

                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input class="form-control" type="text" name="last_name" id="last_name" value="" >
                </div>

                <div class="form-group">
                    <label for="email">Password</label>
                    <input class="form-control" type="password" name="password" id="password" value="" >
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Sign Up</button>

            </form>
        </div>
    </div>




@endsection
