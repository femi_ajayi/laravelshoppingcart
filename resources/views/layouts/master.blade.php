<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> @yield('title') </title>

   {{-- <link rel="stylesheet" href="{{ URL::to('lib/bootstrap-4/css/bootstrap.min.css') }}">--}}
    <link rel="stylesheet" href="{{ URL::to('lib/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('lib/jquery-ui/jquery-ui.theme.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('src/css/app.css') }}">
    @yield('styles')
</head>
<body>

{{--@include('includes.header')--}}
@include('partials.header')
<div class="container" style="min-height:85vh;">
    @yield('content')
</div>




<div class="container">

    <hr>
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p><small>Copyright &copy;</small> Mindbloom Limited <span id="currentYear"></span></p>
            </div>
        </div>
    </footer>
</div>
<script>
    currentYear.textContent = new Date().getFullYear();
</script>
<script type="text/javascript" src="{{ URL::to('lib/jquery/jquery-2.2.3.min.js') }}" ></script>
{{--<script type="text/javascript" src="{{ URL::to('lib/bootstrap-4/js/bootstrap.min.js') }}" ></script>--}}
<script type="text/javascript" src="{{ URL::to('lib/bootstrap/js/bootstrap.min.js') }}" ></script>
<script type="text/javascript" src="{{ URL::to('lib/jquery-ui/jquery-ui.min.js') }}" ></script>
<script type="text/javascript" src="{{ URL::to('src/js/app.js') }}"></script>
@yield('scripts')
</body>
<script>

</script>



</html>
